<?php
/*

  type: layout
  content_type: static
  name: Home
  position: 11
  description: Home layout

 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div class="edit"  rel="page" field="acasa-home">

    <module type="slider" template="one" />

    <module type="layouts" template="services" />
    <module type="layouts" template="about" />
    <module type="layouts" template="team" />
    <module type="layouts" template="recenzii" />

    
    <module type="posts" template="home-posts" />






    


</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
