
<!-- Page Footer-->
<section class="section section-sm bg-white-lighter edit" rel="content" field="footer-content">
    <div class="shell shell-out">
        <div class="range range-30">
            <div class="cell-sm-3 cell-xs-5">
                <div class="preffix-xl-70" style="max-width: 274px">
                    <h6 class="text-spacing-200 text-uppercase font-base">Contact</h6>
                    <div class="divider-modern"></div>
                    <ul class="list list-md">
                        <li>
                            <div class="unit unit-spacing-xs unit-horizontal unit-custom">
                                <div class="unit-left"><span class="icon icon-md icon-primary mdi-phone"></span></div>
                                <div class="unit-body"><a class="link-gray-darker" href="tel:#">+40 (368) 987–587</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="unit unit-spacing-xs unit-horizontal unit-custom">
                                <div class="unit-left"><span class="icon icon-md icon-primary mdi-email-outline"></span></div>
                                <div class="unit-body" style="position: relative; top: 1px"><a class="link-gray-darker" href="mailto:">contact@mail.ro</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="cell-sm-3 cell-xs-5">
                <div class="preffix-xl-70" style="max-width: 274px">
                    <h6 class="text-spacing-200 text-uppercase font-base">Adresa</h6>
                    <div class="divider-modern"></div>
                    <ul class="list list-md">
                        <li>
                            <div class="unit unit-spacing-xs unit-horizontal">
                                <div class="unit-left"><span class="icon icon-md-biger icon-primary mdi-map-marker"></span></div>
                                <div class="unit-body" style="position: relative; top: -4px;"><a class="link-default" href="#">Bucuresti, str. Obor 12</a></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="cell-sm-4">
                <div class="preffix-xl-70" style="max-width: 433px">
                    <h6 class="text-spacing-200 text-uppercase font-base">Nume clinica</h6>
                    <div class="divider-modern"></div>
                    <p>
                        Există o mulţime de variaţii disponibile ale pasajelor Lorem Ipsum, dar majoritatea lor au suferit alterări într-o oarecare măsură prin infiltrare de elemente de umor.


                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="page-footer section text-center">
    <div class="shell shell-out">
        <div class="range range-xs-reverse range-10 edit" rel="global" field="main-footer">
            <div class="cell-xs-4 cell-sm-4 text-xs-right">
                <div class="preffix-xl-70" style="max-width: 433px">
                    <module type="social_links" template="footer" />
                </div>
            </div>
            <div class="cell-xs-6 cell-sm-6 text-xs-left">
                <p class="copyright preffix-xl-70">Copyright &copy; <span class="unselectable" contentEditable="false"><?php print date('Y'); ?></span>, All rights reserved.
                </p>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- PANEL-->
<!-- END PANEL-->
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="<?php print TEMPLATE_URL; ?>js/core.min.js"></script>
<script src="<?php print TEMPLATE_URL; ?>js/script.js"></script>
</body>
</html>