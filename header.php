<!DOCTYPE HTML>
<html class="wide wow-animation" prefix="og: http://ogp.me/ns#">
    <head>
        <title>{content_meta_title}</title>

        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">

        <!--  Site Meta Data  -->
        <meta name="keywords" content="{content_meta_keywords}">
        <meta name="description" content="{content_meta_description}">

        <!--  Site Open Graph Meta Data  -->
        <meta property="og:title" content="{content_meta_title}">
        <meta property="og:type" content="{og_type}">
        <meta property="og:url" content="{content_url}">
        <meta property="og:image" content="{content_image}">
        <meta property="og:description" content="{og_description}">
        <meta property="og:site_name" content="{og_site_name}">

        <link rel="icon" href="<?php print TEMPLATE_URL; ?>images/favicon.png" type="image/x-icon">
        <!-- Stylesheets-->
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:400,700%7CQuattrocento+Sans:400,700">
        <link rel="stylesheet" href="<?php print TEMPLATE_URL; ?>css/bootstrap.css">
        <link rel="stylesheet" href="<?php print TEMPLATE_URL; ?>css/fonts.css">
        <link rel="stylesheet" href="<?php print TEMPLATE_URL; ?>css/style.css">
        <!--[if lt IE 10]>
<div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
<script src="js/html5shiv.min.js"></script>
        <![endif]-->


    </head>
    


        <!-- Page-->
        <div class="text-left page">
            <!-- Page preloader-->
            <div class="page-loader">
                <div>
                    <div class="page-loader-body">
                        <div class="heartbeat"></div>
                    </div>
                </div>
            </div>


            <!-- Page Header-->
            <header class="page-header">
                <!-- RD Navbar-->
                <div class="rd-navbar-wrap">
                    <nav rel="global" field="header-main" class="edit nodrop rd-navbar rd-navbar-default" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="49px" data-lg-stick-up-offset="46px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
                        <div class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
                        <div class="rd-navbar-top-panel" style="max-height: 49px;">
                            <div class="rd-navbar-top-panel-inner">
                                <div class="rd-navbar-collapse toggle-original-elements">
                                    <ul class="list-spreader">
                                        <li>Sector 1, Bucuresti, Judet Bucuresti</li>
                                        <li>Luni - Vineri: 8:00–20:00  Weekend: Inchis</li>
                                    </ul>
                                    <div class="rd-navbar-call">
                                        <div class="unit-link-with-icon unit unit-spacing-xs unit-horizontal">
                                            <div class="unit-left"><span class="icon icon-md-big icon-primary mdi-phone"></span></div>
                                            <div class="unit-body"><a href="tel:#">+40-21-123-4567</a></div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <!--RD Navbar Search-->
                                <div class="rd-navbar-search toggle-original-elements">
                                    <span class="rd-navbar-search-tooltip"> Clinica stomatologica</span>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="rd-navbar-inner">
                            <!-- RD Navbar Panel-->
                            <div class="rd-navbar-panel">
                                <!-- RD Navbar Toggle-->
                                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                                <!-- RD Navbar Brand-->
                                <module type="logo" id="logo" name="header_logo" template="single" class="rd-navbar-brand"  />
                            </div>
                            <div class="rd-navbar-aside-right">
                                <module type="menu" class="rd-navbar-nav-wrap rd-navbar-nav-wrap-default" nname="header_menu" id="main-navigation" template="one"  />  
                            </div>
                        </div>
                    </nav>
                </div>
            </header>