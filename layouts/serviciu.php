<?php
/*

  type: layout
  content_type: static
  name: Pagina Serviciu
  position: 2

  description:

 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div id="content" class="shell">

    <module type="breadcrumb" />


    <section class="section section-xs bg-white edit" rel="content" field="services-content">
        <div class="shell">
            <div class="range range-60">
                <div class="cell-lg-8">


                    <div class="range range-md-middle range-30">
                        <div class="cell-md-6">
                            <div class="image-skew" style="max-width: 803px;">
                                <div class="image-skew-inner">
                                    <div class="image-skew-inner-img"><img src="<?php print TEMPLATE_URL; ?>images/about-01-803x458.jpg" alt="" width="803" height="458"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md-4">
                            <h1 class="heading-with-aside-divider edit" field="title" rel="content">Titlul paginii<span class="divider"></span></h1>
                            <div class="text-gray-darker">
                                E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                                <br><div class="divider-default"></div><br>
                                Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                            </div>
                        </div>
                    </div>

                    <div class="block-text-width-divider">
                        <div class="block-text-width-divider-content">
                            <p class="text-gray-darker">
                                E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                            </p>

                        </div>
                    </div>
                </div>
                <div class="cell-lg-2">
                    <div class="aside-info">
                        <div class="aside-info-item">
                            <h4>Servicii</h4>
                            <module type="pages" parent="45" />


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
