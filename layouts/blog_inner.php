<?php include THIS_TEMPLATE_DIR. "header.php"; ?>
<div class="shell" id="blog-container">
    
    <module type="breadcrumb" />
    
                    <div class="range range-md-middle range-30">
                        <div class="cell-md-6">
                            
                            <module data-type="pictures" data-template="blog"  rel="content"  />
                        </div>
                        <div class="cell-md-4">
                            <h1 class="heading-with-aside-divider edit" field="title" rel="content">Titlul paginii<span class="divider"></span></h1>
                            <div class="text-gray-darker edit post-content" field="content" rel="content">
                                E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                                <br><div class="divider-default"></div><br>
                                Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                            </div>
                        </div>
                    </div>
    
    <module type="posts" template="home-posts" />
</div>
<?php include   TEMPLATE_DIR.  "footer.php"; ?>