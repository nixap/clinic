<?php
/*

  type: layout
  content_type: static
  name: Contact Us

  description: Contact us layout
  position: 7
 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div id="content" >
    <div class="shell" style="padding: 0 15px;">


        <section class="section section-lg bg-white edit" field="contact" rel="content">
            <div class="shell">
                <div class="range range-30">
                    <div class="cell-md-5">
                        <div class="form-block">
                            <h3>Get in Touch</h3>
                            <p>We are available 24/7 by fax, e-mail or by phone. You can also use our quick<br class="veil reveal-lg-block"> contact form to ask a question about our services that we regularly provide.
                            </p>
                            
                            
                            <module type="contact_form" template="basic" class="contact-form" id="contact-form" />
                        </div>
                    </div>
                    <div class="cell-md-5">
                       <module type="google_maps" style="height: 100%;" />
                    </div>
                </div>
            </div>
        </section>



        



        </div>
</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
