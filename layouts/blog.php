<?php
/*

  type: layout
  content_type: dynamic
  name: Blog
  position: 3
  description: Blog

 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div id="content" class="shell">
    <module type="breadcrumb" />
    <module data-type="posts" limit="6" data-template="blog" data-page-id="<?php print CONTENT_ID ?>"  />
</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
