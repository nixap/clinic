<?php
/*

  type: layout
  content_type: static
  name: Despre noi

  description: About us layout
  position: 1
 */
?>
<?php include THIS_TEMPLATE_DIR . "header.php"; ?>

<div id="content" class="shell">

    <module type="breadcrumb" />


    <section class="section section-xs bg-white edit" rel="content" field="page-content">
        <div class="shell">
            <div class="range range-60">
                <div class="cell-lg-12">
                    <module type="layouts" template="about" />
                    <div class="block-text-width-divider">
                        <div class="block-text-width-divider-content">
                            <p class="text-gray-darker">
                                E un fapt bine stabilit că cititorul va fi sustras de conţinutul citibil al unei pagini atunci când se uită la aşezarea în pagină. Scopul utilizării a Lorem Ipsum, este acela că are o distribuţie a literelor mai mult sau mai puţin normale, faţă de utilizarea a ceva de genul "Conţinut aici, conţinut acolo", făcându-l să arate ca o engleză citibilă. 
                            </p>

                        </div>
                    </div>
                    <module type="layouts" template="services" />

                </div>
            </div>
        </div>
    </section>


</div>
<?php include THIS_TEMPLATE_DIR . "footer.php"; ?>
