<?php
/*

  type: layout

  name: Default

  description: Default


 */
?>

<?php if (isset($data) and is_array($data)): ?>
    <section class="section section-breadcrumb">
        <ul class="breadcrumb-custom">
            <li><a href="<?php print(site_url()); ?>"><?php print _e('Home'); ?></a></li>
            <?php foreach ($data as $item): ?>
                <?php if (!($item['is_active'])): ?>
                    <li><a href="<?php print($item['url']); ?>"><?php print($item['title']); ?></a></li>
                <?php else: ?>
                    <li><?php print($item['title']); ?></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </section>
<?php endif; ?>