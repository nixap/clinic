<?php
/*

  type: layout

  name: Recenzii

  description: Testimonials displayed in Slider

 */
?>





<!-- About Clinic-->
<section class="section section-lg bg-white wow fadeIn">
    <div class="shell">
        <div class="range range-md-middle range-30">
            <div class="cell-md-6 wow fadeIn" data-wow-delay=".3s">
                <div class="image-skew" style="max-width: 803px;">
                    <div class="image-skew-inner">
                        <div class="image-skew-inner-img"><img src="<?php print TEMPLATE_URL; ?>images/about-01-803x458.jpg" alt="" width="803" height="458"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell-md-4">
                <h2 class="heading-with-aside-divider">Recenzii<span class="divider"></span></h2>
                <div class="owl-carousel" data-items="1" data-stage-padding="0" data-loop="false" data-autoplay="true" data-margin="30" data-mouse-drag="false" data-animation-in="fadeIn" data-dots-custom="#owl-pagination-custom">
                    <?php
                    $i = 0;
                    foreach ($data as $item) {
                        ?>
                        <div class="item">
                            <div class="quote-testimonial">
                                <p class="quote-testimonial-title"><?php print $item['content']; ?></p>
                                <p class="quote-testimonial-cite"><?php print $item['name']; ?></p>
                            </div>
                        </div>
                        <?php $i++;
                    }
                    ?>
                </div>
                <div id="owl-pagination-custom">
<?php
                    $i = 0;
                    foreach ($data as $item) {
                        ?>
                    <div class="owl-dot-custom wow fadeIn" data-wow-delay=".<?php print $i+1; ?>s" data-owl-item="<?php print $i; ?>"><img class="img-circle" src="<?php print $item['client_picture']; ?>" alt="" width="80" style="height:80px;"/>
                    </div>

                        <?php $i++;
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>