<?php
/*

  type: layout

  name: Main

  description: Small Navigation

 */


$menu_filter['ul_class'] = 'rd-navbar-nav';
$menu_filter['ul_class_deep'] = 'rd-navbar-dropdown';
$menu_filter['li_class_empty'] = ' ';
$menu_filter['a_class'] = '';

$mt = menu_tree($menu_filter);
if ($mt != false) {
    print ($mt);
} else {
    print lnotif(_e('There are no items in the menu', true) . " <b>" . $params['menu-name'] . '</b>');
}
?>

<div class="rd-navbar-call">
    <div class="unit-link-with-icon unit unit-spacing-xs unit-horizontal">
        <div class="unit-left"><span class="icon icon-md-big icon-primary mdi-phone"></span></div>
        <div class="unit-body"><a href="tel:#">+40-21-123-4567</a></div>
    </div>
</div>