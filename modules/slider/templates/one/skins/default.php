


                <div class="swiper-slide" data-slide-bg="<?php print $slide['images'][0]; ?>">
                    <div class="swiper-slide-caption">
                        <div class="parallax-container parallax-container-modern" data-parallax-img="">
                            <div class="parallax-content section-lg">
                                <p class="heading-4 heading-divider-modern" data-caption-animate="fadeInUp" data-caption-delay="250" style="font-weight: 400"><?php print $slide['secondaryText']; ?></p>
                                <h1 data-caption-animate="fadeInUp" data-caption-delay="100"><?php print $slide['primaryText']; ?></h1><a class="button button-white button-effect-ujarak" href="<?php if (isset($slide['url'])) {
                            print $slide['url'];
                        } ?>" data-caption-animate="fadeInUp" data-caption-delay="450"><?php print $slide['seemoreText'] ?></a>
                            </div>
                        </div>
                    </div>
                </div>