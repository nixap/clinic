<?php
/*

  type: layout

  name: With Socials - Default

  description: Slick Slider - Default


 */
?>
<section class="swiper-main-wrap wow fadeIn" >
    <div class="swiper-container swiper-slider" data-index-bullet="true" data-clickable="true" data-custom-pagination="#swiper-pagination-index" data-slide-effect="fade" data-autoplay="4000" data-simulate-touch="false">
        <div class="swiper-wrapper">


            <?php foreach ($data as $slide) { ?>
                <?php if (isset($slide['skin_file'])) { ?>
                    <?php include $slide['skin_file'] ?>
                <?php } ?>
            <?php } ?>


        </div>
    </div>
    <div class="swiper-main-list-wrap">
        <ul class="swiper-main-list edit" field="slide-socials">
            <li><a class="icon fa-facebook icon-white-lighter-block" href="#"></a></li>
            <li><a class="icon fa-twitter icon-white-lighter-block" href="#"></a></li>
            <li><a class="icon icon-xxs fa-google-plus icon-white-lighter-block" href="#"></a></li>
        </ul>
    </div>
    <div class="swiper-pagination swiper-pagination-index" id="swiper-pagination-index"></div>
</section>