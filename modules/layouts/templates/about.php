<?php

/*

type: layout

name: About

description: About Default

*/

?>


    <section class="section section-lg bg-white wow fadeIn">
        <div class="shell">
            <div class="range range-md-middle range-30 edit" rel="content" field="home-about">
                <div class="cell-md-6 wow fadeIn" data-wow-delay=".3s">
                    <div class="image-skew" style="max-width: 803px;">
                        <div class="image-skew-inner">
                            <div class="image-skew-inner-img"><img src="<?php print TEMPLATE_URL; ?>images/about-02-803x458.jpg" alt="" width="803" height="458"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell-md-4">
                    <h2 class="heading-with-aside-divider wow fadeInUp" data-wow-delay=".1s">About Clinic<span class="divider"></span></h2>
                    <p class="wow fadeInUp" data-wow-delay=".3s">The Medical private medical clinic specializes in a wide variety of health issues, related to absolutely any age or severity level. Our seasoned team of highly trained physicians and practical nurses will be glad to help you 24/7!</p>
                    <div class="range range-40 range-lg-50 range-lg safe-mode">
                        <div class="cloneable cell-xs-5">
                            <div class="icon-unit wow fadeInUp" data-wow-delay=".7s">
                                <div class="icon-unit-left"><i class="fa fa-address-book"></i><img src="<?php print TEMPLATE_URL; ?>images/icon-01-34x52.png" alt="" width="34" height="52"/>
                                </div>
                                <div class="icon-unit-body">
                                    <h5>Qualified Doctors</h5>
                                    <p>25 years of experience</p>
                                </div>
                            </div>
                        </div>
                        <div class="cloneable cell-xs-5">
                            <div class="icon-unit wow fadeInUp" data-wow-delay=".75s">
                                <div class="icon-unit-left"><img src="<?php print TEMPLATE_URL; ?>images/icon-02-33x52.png" alt="" width="33" height="52"/>
                                </div>
                                <div class="icon-unit-body">
                                    <h5>Modern Equipment</h5>
                                    <p>Branded and time tested</p>
                                </div>
                            </div>
                        </div>
                        <div class="cloneable cell-xs-5">
                            <div class="icon-unit wow fadeInUp" data-wow-delay=".8s">
                                <div class="icon-unit-left"><img src="<?php print TEMPLATE_URL; ?>images/icon-03-39x39.png" alt="" width="39" height="39"/>
                                </div>
                                <div class="icon-unit-body">
                                    <h5>Emergency Help</h5>
                                    <p>Get help anytime</p>
                                </div>
                            </div>
                        </div>
                        <div class="cloneable cell-xs-5">
                            <div class="icon-unit wow fadeInUp" data-wow-delay=".85s">
                                <div class="icon-unit-left"><img src="<?php print TEMPLATE_URL; ?>images/icon-04-36x43.png" alt="" width="36" height="43"/>
                                </div>
                                <div class="icon-unit-body">
                                    <h5>Individual Approach</h5>
                                    <p>Each case is always special</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
print lnotif('Click here to select layout');
?>