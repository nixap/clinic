


<!-- Our Doctors-->
    <section class="section section-lg bg-white wow fadeIn">
        <div class="shell">
            <div class="range">
                <div class="cell-xs-10">
                    <div class="box">
                        <div class="box-left">
                            <h2 class="edit">Medicii nostri</h2>
                        </div>
                        <div class="box-right"><a class="button button-albus button-effect-ujarak" href="#"> vezi pe toti</a></div>
                    </div>
                    <div class="divider-default"></div>
                    <div class="row range-30">
                       <?php
    $count = 0;
    if (isset($data) AND $data) {
        foreach ($data as $slide) {
            $count++;
            ?>
                        <div class="col-sm-6 col-lg-4">
                             
                            <div class="block-disclosure wow fadeIn" data-wow-delay=".1s">
                                <div class="block-disclosure-left"><img src="<?php print thumbnail($slide['file'], 200); ?>" alt="" width="151" height="203"/>
                                </div>
                                <div class="block-disclosure-body">
                                    <p class="block-disclosure-cite"><?php print array_get($slide, 'role'); ?></p>
                                    <h5 class="block-disclosure-title"><a href="#"><?php print array_get($slide, 'name'); ?></a></h5>
                                    <div class="block-disclosure-divider"></div>
                                    <p class="block-disclosure-info"><?php print array_get($slide, 'bio'); ?></p>
                                </div>
                            </div>
                 
                            
                        </div>           
        <?php }
    } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
 
