<?php
/*

  type: layout

  name: Inner

  description: Inner Slider

 */
?>

<?php if (is_array($data)): ?>
    <?php foreach ($data as $item): ?>


        <div class="image-skew" style="max-width: 803px;">
            <div class="image-skew-inner">
                <div class="image-skew-inner-img"><img src="<?php print thumbnail($item['filename'], 1400); ?>" alt="" width="803" height="458"/>
                </div>
            </div>
        </div>
        <?php break; ?>
    <?php endforeach; ?>

<?php else : ?>
<?php endif; ?>