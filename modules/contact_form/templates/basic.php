<?php

/*

type: layout

name: Basic

description: Basic contact form

*/

?>

<script>
    mw.moduleCSS("<?php print $config['url_to_module']; ?>css/style.css", true);
</script>

    <form class="rd-mailform" data-form-id="<?php print $form_id ?>" name="<?php print $form_id ?>" method="post">


        <module type="custom_fields" data-template="bootstrap4" for-id="<?php print $params['id'] ?>" data-for="module" default-fields="name,email,message"/>
        
        <?php if (get_option('disable_captcha', $params['id']) != 'y'): ?>
            <div class="control-group form-group">
                <label><?php _e("Security code"); ?></label>
                <div class="mw-ui-row captcha-holder">
                    <div class="mw-ui-col">
                        <module type="captcha"/>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        
        <module type="btn" button_action="submit" button_style="btn btn-default" button_text="<?php _e("Submit"); ?>"  />
        
    </form>
