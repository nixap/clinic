<?php

/*

type: layout

name: Footer

description: Default

*/
?>
                    <ul class="list-inline">
                        <?php if ($facebook_enabled) { ?>
                        <li><a class="icon icon-gray-darker fa-facebook" href="<?php print $facebook_url; ?>"></a></li>
                        <?php } ?>
                        <?php if ($twitter_enabled) { ?>
                        <li><a class="icon icon-gray-darker fa-twitter" href="<?php print $twitter_url; ?>"></a></li>
                        <?php } ?>
                        <?php if ($instagram_enabled) { ?>
                        <li><a class="icon icon-gray-darker fa-instagram" href="<?php print $instagram_url; ?>"></a></li>
                        <?php } ?>
                    </ul>