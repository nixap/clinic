


 <div class="swiper-slide" data-slide-bg="<?php print TEMPLATE_URL; ?>images/slide-01.jpg">
                    <div class="swiper-slide-caption">
                        <div class="parallax-container parallax-container-modern" data-parallax-img="">
                            <div class="parallax-content section-lg">
                                <p class="heading-4 heading-divider-modern" data-caption-animate="fadeInUp" data-caption-delay="250" style="font-weight: 400">Individual approach to each patient</p>
                                <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Medical Service</h1><a class="button button-white button-effect-ujarak" href="contacts.html" data-caption-animate="fadeInUp" data-caption-delay="450">make an Appointment</a>
                            </div>
                        </div>
                    </div>
                </div>