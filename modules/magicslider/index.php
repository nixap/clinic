<?php
$settings = get_option('settings', $params['id']);

$defaults = array(
    'images' => '',
    'primaryText' => 'A Magic Slider',
    'secondaryText' => 'Nunc blandit malesuada.',
    'seemoreText' => 'See more',
    'url' => '',
    'urlText' => '',
    'skin' => 'default'
);

$settings = get_option('settings', $params['id']);
$json = json_decode($settings, true);

if (isset($json) == false or count($json) == 0) {
    $json = array(0 => $defaults);
}

$mrand = 'slider-' . uniqid();


?>

<?php print lnotif("Click here to manage slides"); ?>




<section class="swiper-main-wrap wow fadeIn"  id="<?php print $mrand; ?>">
        <div class="swiper-container swiper-slider" data-index-bullet="true" data-clickable="true" data-custom-pagination="#swiper-pagination-index" data-slide-effect="fade" data-autoplay="4000" data-simulate-touch="false">
            <div class="swiper-wrapper">
                
                <?php
        foreach ($json as $slide) {
            if (!isset($slide['skin']) or $slide['skin'] == '') {
                $slide['skin'] = 'default';
            }

            if (isset($slide['images'])) {
                $slide['images'] = is_array($slide['images']) ? $slide['images'] : explode(',', $slide['images']);
            } else {
                $slide['images'] = array();
            }

            if (!isset($slide['seemoreText'])) {
                $slide['seemoreText'] = 'See more';
            }

            $skin_file  = $config['path_to_module'] . 'skins/' . $slide['skin'] . '.php';
            $skin_file_from_template= template_dir() . 'modules/magicslider/skins/'. $slide['skin'] . '.php';

            $skin_file = normalize_path($skin_file,false);
            $skin_file_from_template = normalize_path($skin_file_from_template,false);

            if(is_file($skin_file_from_template)){
                include ($skin_file_from_template);
            } elseif(is_file($skin_file)){
                include ($skin_file);
            } else {
                print lnotif('Skin file is not found.');
            }

        }
        ?>
                
                
                <div class="swiper-slide" data-slide-bg="<?php print TEMPLATE_URL; ?>images/slide-01.jpg">
                    <div class="swiper-slide-caption">
                        <div class="parallax-container parallax-container-modern" data-parallax-img="">
                            <div class="parallax-content section-lg">
                                <p class="heading-4 heading-divider-modern" data-caption-animate="fadeInUp" data-caption-delay="250" style="font-weight: 400">Individual approach to each patient</p>
                                <h1 data-caption-animate="fadeInUp" data-caption-delay="100">Medical Service</h1><a class="button button-white button-effect-ujarak" href="contacts.html" data-caption-animate="fadeInUp" data-caption-delay="450">make an Appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
        <div class="swiper-main-list-wrap">
            <ul class="swiper-main-list">
                <li><a class="icon fa-facebook icon-white-lighter-block" href="#"></a></li>
                <li><a class="icon fa-twitter icon-white-lighter-block" href="#"></a></li>
                <li><a class="icon icon-xxs fa-google-plus icon-white-lighter-block" href="#"></a></li>
            </ul>
        </div>
        <div class="swiper-pagination swiper-pagination-index" id="swiper-pagination-index"></div>
    </section>