<?php

/*

type: layout

name: Services

description: Default

*/
?>

<?php


$tn = $tn_size;
if (!isset($tn[0]) or ($tn[0]) == 52) {
    $tn[0] = 33;
}
if (!isset($tn[1])) {
    $tn[1] = $tn[0];
}


?>
<?php
$only_tn = false;


$search_keys = array('title', 'created_at', 'description', 'read_more');

if (isset($show_fields) and is_array($show_fields) and !empty($show_fields)) {
    $only_tn = true;
    foreach ($search_keys as $search_key) {
        foreach ($show_fields as $show_field) {
            if ($search_key == $show_field) {
                $only_tn = false;
            }
        }
    }


}

?>


<!-- Departaments-->
    <section class="section section-lg bg-white wow fadeIn">
        <div class="shell">
            <div class="range">
                <div class="cell-xs-10">
                    <div class="box edit" rel="content" field="services-title">
                        <div class="box-left">
                            <h2>Servicii</h2>
                        </div>
                        <div class="box-right">
                            <div class="owl-outer-navigation" id="owl-carousel-nav">
                                <div class="owl-arrow owl-arrow-prev fl-budicons-free-left161"></div>
                                <div class="owl-arrow owl-arrow-next fl-budicons-free-right163"></div>
                            </div><a class="button button-albus button-effect-ujarak" href="#"> Toate serviciile</a>
                        </div>
                    </div>
                    <div class="divider-default"></div>
                    <div class="owl-carousel" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="5" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-nav-custom="#owl-carousel-nav" data-autoplay="true">
                        <?php foreach ($data as $item): ?>
                        <div class="item wow fadeInRight" data-wow-delay=".1s">
                            <a class="block-accent" href="<?php print $item['link'] ?>">
                                <div class="block-accent-inner">
                                    <img src="<?php print thumbnail($item['image'], $tn[0], $tn[1]); ?>" alt="" width="33" height="52"/>
                                    <h4 class="block-accent-title"><?php print $item['title'] ?></h4>
                                </div>
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>