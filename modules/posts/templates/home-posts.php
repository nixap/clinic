<?php

/*

type: layout

name: Din Blog

description: Default

*/
?>






    <!-- Latest News-->
    <section class="section section-lg bg-white wow fadeIn">
        <div class="shell">
            <div class="range">
                <div class="cell-xs-10">
                    <div class="box">
                        <div class="box-left">
                            <h2>Recent din blog</h2>
                        </div>
                        <div class="box-right">
                            <div class="owl-outer-navigation" id="owl-carousel-nav-1">
                                <div class="owl-arrow owl-arrow-prev fl-budicons-free-left161"></div>
                                <div class="owl-arrow owl-arrow-next fl-budicons-free-right163"></div>
                            </div>
                        </div>
                    </div>
                    <div class="divider-default"></div>
                    <div class="owl-carousel" data-items="1" data-md-items="2" data-stage-padding="0" data-loop="true" data-autoplay="true" data-margin="30" data-mouse-drag="false" data-nav-custom="#owl-carousel-nav-1">
                        <?php foreach ($data as $item): ?>
                        <div class="item wow fadeIn" data-wow-delay=".1s">
                            <article class="post">
                                <div class="post-header">
                                    <div class="post-header-inner">
                                        <div class="post-header-inner-img"><a href="<?php print $item['link'] ?>"><img src="<?php print thumbnail($item['image'], 719, null); ?>" alt="" width="719" height="246"/></a></div>
                                    </div>
                                </div>
                                <div class="post-footer">
                                    <h4><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h4>
                                    <div class="post-meta">
                                        <dl>
                                            <dt>Data</dt>
                                            <dd><a href="<?php print $item['link'] ?>">
                                                    <time datetime="2018-01-22"><?php print $item['created_at'] ?></time></a></dd>
                                        </dl>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>