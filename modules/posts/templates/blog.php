<?php
/*

  type: layout

  name: Din Blog

  description: Default

 */
?>






<!-- Latest News-->
<section class="section section-lg bg-white wow fadeIn">
    <div class="shell">
        <div class="range">

            <?php foreach ($data as $item): ?>
                <div class="col-md-6">
                    <article class="post">
                        <div class="post-header">
                            <div class="post-header-inner">
                                <div class="post-header-inner-img"><a href="<?php print $item['link'] ?>"><img src="<?php print thumbnail($item['image'], 719, null); ?>" alt="" width="719" height="246"/></a></div>
                            </div>
                        </div>
                        <div class="post-footer">
                            <h4><a href="<?php print $item['link'] ?>"><?php print $item['title'] ?></a></h4>
                            <div class="post-meta">
                                <dl>
                                    <dt>Data</dt>
                                    <dd><a href="<?php print $item['link'] ?>">
                                            <time datetime="2018-01-22"><?php print $item['created_at'] ?></time></a></dd>
                                </dl>
                            </div>
                        </div>
                    </article>
                </div>
            <?php endforeach; ?>

            <ul class="pagination-custom">
                <?php if (isset($pages_count) and $pages_count > 1 and isset($paging_param)): ?>
                <li><?php print paging("num={$pages_count}&paging_param={$paging_param}&current_page={$current_page}") ?></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>

